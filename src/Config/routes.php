<?php

return [
    'path' => app_path('Http/**/*.php'),
    'prefix_namespace' => 'App\\',
    'other_routes' => []
];
