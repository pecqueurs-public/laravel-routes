# Laravel Routes

## objective

Easily organize routes in project by feature.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-routes
```

### Publish config

One shot :
```bash
php artisan vendor:publish --provider='PecqueurS\LaravelRoutes\Providers\LaravelRoutesProvider' --tag='config' --ansi
```

in composer.json for each update :

```json
...
"scripts": {
    ...
    "post-update-cmd": [
        ...
        "@php artisan vendor:publish --provider='PecqueurS\\LaravelRoutes\\Providers\\LaravelRoutesProvider' --tag='config' --ansi"
    ],
    ...
},
...
```

### Usage

#### Init

in `./routes/api.php`:

```php
(new \PecqueurS\LaravelRoutes\Routes\RouteController())->handle();
```

All routes are loaded from route class extends `PecqueurS\LaravelRoutes\Routes\AbstractRouteController`. 

#### Config

**path** => Define folder where find `AbstractRouteController` children. (Ex: `app_path('Http/Routes/*Route.php')` or `app_path('Services/**/Routes/*.php')`)  
**prefix_namespace** => prefix to use before path (Ex: `'App\\'`)  
**other_routes** => other `AbstractRouteController` children not found in path folders.  

#### Implement `AbstractRouteController` child

```php

namespace App\Http\Routes;

use Illuminate\Support\Facades\Route;
use PecqueurS\LaravelRoutes\Routes\AbstractRouteController;

class MyRoute extends AbstractRouteController
{
    public function handle()
    {
        Route::middleware('my-middleware')->group(function() {
            Route::get('route', [MyController::class, 'index']);
        });

        Route::get('route2', [MyController::class, 'show']);
    }
}

```
